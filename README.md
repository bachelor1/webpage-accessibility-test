# webpage-accessibility-test

Continous Integration / Continious Development project to  verify the accessibility of a given webpage(s)

Creates docker container from gitlab's templates
Documentation: https://docs.gitlab.com/ee/user/project/merge_requests/accessibility_testing.html

And runs Pa11y's tests on the website(s)
Documentation: https://pa11y.org/
